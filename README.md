# Developments for the working environment GNOME
INTRODUCTION:
Here are various versions of OS Linux based on the example of Debian 9.4, they will also work well on all distributions.
# How to start?
You can download the finished, [collected project](https://github.com/MVDraurus/VSCodeLaboratory/releases) with extension .out, ither to build the project by downloading the version to your local repository.

## Install git

To download the project to a local repository, you must have git installed.
To install it, run the command with the correct super user -
```
### Debian, Ubuntu, Kali, Mint
$ sudo apt-get update
$ sudo apt-get install git
```
```
### Fudora, RedHat
$ yum -y update
$ yum -y install git
```

To check if git is installed, check its version with the command:
```
$ git --version
```
in case of successful installation you will see about the following lines:
```
git version 2.17.0
```

### Install compiler

To do this, you need to install the compiler, running a command with superuser privileges -
```
# Debian, Ubuntu, Kali, Mint
$ sudo apt-get install g++
```
```
### Fudora, RedHat
$ yum install gcc-c++
or
$ sudo dnf install gcc-c++
```

To verify that the compiler is installed, check its version using the command:
```
$ g++ --version
```
in case of successful installation you will see about the following lines:
```
g++ (Debian 6.3.0-18+deb9u1) 6.3.0 20170516
```
### Compilation

In order to compile the project:
Go to the project folder by running the command-
```
$ cd ~/Download
```
or where to where it is stored, then compile the command-
```
$ g++ -g main.cpp -o main.out
```
after successful compilation, the executable file main.out should appear in the current directory,
to run it, run the command-
```
$ ./main.out
```
### P.S >

I'm a beginner programmer, so do not judge me for any mistakes, but show me where they are and show advice!

I will be very grateful for every comment to my developments, and gladly receive your advice!

I hope that my developments, at least somehow help someone!
