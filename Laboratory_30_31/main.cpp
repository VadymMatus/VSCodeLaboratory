/*
	Завдання: уводити дані відповідно наведеній нижче структурі. Введені дані зберігати у масиві структур.

	А.Описати структуру з ім'ям TRAIN, що містить наступні поля:
        • Назва пункту призначення;
        • Номер поїзда;
        • Час відправлення.
    Ввести з клавіатури дані про 10 поїздах,
    Б. Записи розмістити (і вивести в таблицю) в алфавітному порядку за номерами поїздів;
    В. Написати програму, що виконує наступні дії:
        • Висновок на екран інформації про потяг, номер якого введено з клавіатури;
        • Якщо таких поїздів немає, видати на дисплей видповидне повідомлення.
*/

#include <iostream>
#include <string.h>
#include <limits>
#include <termios.h>
#include <iomanip> // setw()

using namespace std;

#define NumberRoutes 5      /* количество записей */
#define SizeDestination 256 /* длина масива Destination */

struct Train
{
    char Destination[SizeDestination]; /// Пункт назначения
    long int Number = 0;               /// номер поезда
    long int PoisonTime[2] = {0, 0};   /// время отправления [0]=часы [1]=минуты
};

void FunClearBuffer() /* Очистка буфера ввода */
{
    cin.clear(); // на случай, если предыдущий ввод завершился с ошибкой
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
}

int FunMenu();                          // Функция вывода меню
int FunFillStruct(Train *Tp);           // Функция ввода данных
void FunShowData(Train *Tp, bool more); // Функция вывода данных
void FunSortingStruct(Train *Tp);       // Функция сортировки структуры

int main()
{

    bool flag(true);

    Train ArrayTrain[NumberRoutes];

    do
    {
        system("clear");
        switch (FunMenu())
        {
        case 1:
        {
            FunFillStruct(ArrayTrain);
            flag = true;
            break;
        }
        case 2:
        {
            FunShowData(ArrayTrain, false);
            flag = true;
            break;
        }
        case 3:
        {
            FunShowData(ArrayTrain, true);
            flag = true;
            break;
        }
        case 4:
        {
            cout << "\033[1;31mЗавершено пользователем... \033[0m" /* <<(system("sleep 1s")) */;
            getchar();

            flag = false;
            break;
        }
        default:
        {
            cout << "\033[1;31m Fatal error!!![0m\n";
            break;
        }
        }

    } while (flag);

    getchar();
    return 0;
}
/*===================================================*\
            ФУНКЦИЯ ВЫВОДА ГЛАВНОГО МЕНЮ
\*===================================================*/
int FunMenu()
{
    int answer(0);
    static bool flag(true);

    cout << "\t\t\t\t\033[1;33m<<< Меню >>>\033[0m\n";
    cout << "1\033[1;33m-> \033[0mВвести данные;\n";
    cout << "2\033[1;33m-> \033[0mВывести информацию о поезде;\n";
    cout << "3\033[1;33m-> \033[0mВывести информацию о всех поездах;\n";
    cout << "4\033[1;33m-> \033[0mВыйти.\n\nВаш выбор \033[1;34m-> \033[0m";

    do
    {
        while (!(cin >> answer))
        {
            cin.clear();
            while (cin.get() != '\n')
                continue;

            cout << "\033[1;31mFatal Error!\033[0m Введите число!\nВаш выбор \033[1;34m-> \033[0m";
        }

        if (flag and answer == 1)
            flag = false;

        if (answer < 1 or answer > 4)
            cout << "\033[1;31mError!\033[0m "
                 << "Неправильный ввод!"
                 << "\nВаш выбор \033[1;34m-> \033[0m";
        else if (flag and answer == 2 or flag and answer == 3)
        {
            cout << "\033[1;31mIncorrectly!\033[0m"
                 << " Вы не можете просмотреть даные, так как ещё не ввели их!!!"
                 << "\nВаш выбор \033[1;34m-> \033[0m";
            getchar();
        }

    } while (answer < 1 or answer > 4 or flag and answer == 2 or flag and answer == 3);

    return answer;
}

/*===================================================*\
                ФУНКЦИЯ ВВОДА ДАНЫХ
\*===================================================*/
int FunFillStruct(Train *Tp)
{
    for (int i = 0; i < NumberRoutes; i++)
    {
        system("clear");

        cout << "\t\t\t\033[1;33m Заполните маршрут №" << i + 1 << "\033[0m\n";

        static bool flag(false);
        static int spare(0);

        getchar();

        do
        {

            cout << "Пункт назначения \033[1;34m-> \033[0m";

            std::cin.getline(Tp[i].Destination, 20);
            // cin >> setw(20) >> Tp[i].Destination;

            FunClearBuffer();
            if (strlen(Tp[i].Destination) == 0 or Tp[i].Destination[0] == '.')
            {
                cout << "\033[1;31mError!\033[0m Строка пустая!\n";
                flag = true;
            }
            else
            {
                spare = 0;

                for (int j(0); j < (strlen(Tp[i].Destination)); j++)
                    if (Tp[i].Destination[j] == ' ' and Tp[i].Destination[j] != '.')
                        spare++;

                if (spare == (strlen(Tp[i].Destination)))
                {
                    cout << "\033[1;31mIncorrectly!\033[0m Строка состоит только из пробелов!\n";

                    flag = true;
                }
                else
                    flag = false;
            }

        } while (flag);

        cout << "Номер поезда \033[1;34m-> \033[0m";

        do
        {
            while (!(cin >> Tp[i].Number))
            {
                cin.clear();
                while (cin.get() != '\n')
                    continue;

                cout << "\033[1;31mFatal Error!\033[0m Введите число!\nНомер поезда \033[1;34m-> \033[0m";
            }
            if (Tp[i].Number < 1)
                cout << "\033[1;31mFatal Error!\033[0m Номер поезда не может быть отрицательным!"
                     << "\nНомер поезда \033[1;34m-> \033[0m";
        } while (Tp[i].Number < 1);

        do
        {
            getchar();

            cout << "Время отправления через пробел \033[1;34m-> \033[0m";

            while (!(cin >> Tp[i].PoisonTime[0] >> Tp[i].PoisonTime[1]))
            {
                cin.clear();
                while (cin.get() != '\n')
                    continue;

                cout << "\033[1;31mFatal Error!\033[0m Введите число!\nВремя отправления через пробел \033[1;34m-> \033[0m ";
            }

            if (Tp[i].PoisonTime[0] > 24 or Tp[i].PoisonTime[0] < 0 or Tp[i].PoisonTime[1] > 59 or Tp[i].PoisonTime[1] < 0)
                cout << "\033[1;31mError!\033[0mВведите коректное время!\n";

        } while (Tp[i].PoisonTime[0] > 24 or Tp[i].PoisonTime[0] < 0 or Tp[i].PoisonTime[1] > 59 or Tp[i].PoisonTime[1] < 0);
    }

    FunSortingStruct(Tp); // сразу сортируем массив структур

    return 0;
}

/*===================================================*\
                ФУНКЦИЯ ВЫВОДА ДАНЫХ
\*===================================================*/
void FunShowData(Train *Tp, bool more)
{
    int distant(0);  // для ровного вывода даных в таблице
    int number(0);      // номер поезда для поиска
    bool flag(true); // есть ли поезд в базе данных

    do
    {
        if (more)
        {
            system("clear");
            cout << "\033[1;33m\t\t\t<<< МАРШРУТЫ ВСЕХ ПОЕЗДОВ >>>\033[0m\v\n";
            getchar();
            break;
        }

        system("clear");

        cout << "\033[1;33m\t\t\t<<< ПОИСК МАРШРУТОВ >>>\033[0m\v\v\rВведите номер поезда \033[1;34m-> \033[0m";

        while (!(cin >> number))
        {
            cin.clear();
            while (cin.get() != '\n')
                continue;

            cout << "\033[1;31mFatal Error!\033[0m Введите число!\nВведите номер поезда \033[1;34m-> \033[0m";
        }

        getchar();

        if (number < 1)
        {
            system("clear");
            cout << "\033[1;31mError!\033[0m Введите коректное число!";
            getchar();
        }

    } while (number < 1);

    /*===================================================*\
                    ВЫВОД ТАБЛИЦЫ С ДАННЫМИ
    \*===================================================*/

    cout << "╔══════════════════════╦══════════════════╦══════════════════════╗";
    cout << "\n║   \033[1;33mПункт назначения\033[0m   ║   \033[1;33mНомер поезда\033[0m   ║   \033[1;33mВремя отправления\033[0m  ║";
    cout << "\n╠══════════════════════╬══════════════════╬══════════════════════╣";

    for (int i = 0; i < NumberRoutes; i++)
    {
        if (!more and Tp[i].Number == number)
        {
            distant = 25 - strlen(Tp[i].Destination);

            cout << "\n║ " << Tp[i].Destination << ((strlen(Tp[i].Destination) > 17) ? "\b\b\b..." : "") << setw(distant) << "║ ";

            cout
                << setw(9) << Tp[i].Number << "\t  ║ "
                << setw(10) << Tp[i].PoisonTime[0] << ((Tp[i].PoisonTime[1] < 10) ? ":0" : ":") << Tp[i].PoisonTime[1] << "\t ║"
                << "\n╠══════════════════════╬══════════════════╬══════════════════════╣";

            flag = false;
        }
        else if (more)
        {
            distant = 25 - strlen(Tp[i].Destination);

            cout << "\n║ " << Tp[i].Destination << ((strlen(Tp[i].Destination) > 17) ? "\b\b\b..." : "") << setw(distant) << "║ ";

            cout
                << setw(9) << Tp[i].Number << "\t  ║ "
                << setw(10) << Tp[i].PoisonTime[0] << ((Tp[i].PoisonTime[1] < 10) ? ":0" : ":") << Tp[i].PoisonTime[1] << "\t ║"
                << "\n╠══════════════════════╬══════════════════╬══════════════════════╣";

            flag = false;
        }
    }

    if (flag)
        cout << "\n║     \033[1;31mНет данных!\033[0m      ║    \033[1;31mНет данных!\033[0m   ║      \033[1;31mНет данных!\033[0m     ║"
             << "\n╚══════════════════════╩══════════════════╩══════════════════════╝\n"
             << "\033[1;31mНет поезда с таким номером!\033[0m\n"
             << "Enter для продолжения...";
    else
        cout << "\r╚══════════════════════╩══════════════════╩══════════════════════╝\n"
             << "Нажмите Enter для продолжения...";

    getchar();
}
/*===================================================*\
        ФУНКЦИЯ СОРТИРОВКИ МАССИВА СТРУКТУР
\*===================================================*/
void FunSortingStruct(Train *Tp)
{
    Train Sorting; // будет хранить елемент масива структур при сортировки

metka:
    for (int i = 0; i < NumberRoutes - 1; i++)
        if (Tp[i].Destination > Tp[i + 1].Destination)
        {
            Sorting = Tp[i];
            Tp[i] = Tp[i + 1];
            Tp[i + 1] = Sorting;

            goto metka;
        }
}
/**            <<< ПЛАН >>>
 * 1. Проверить правильность ввода;(yes)
 * 2. Написать функцию сортировки масива структу;(yes)
 * 3. Оптимизировать програму;
 * 4. Украсить часть видимого кода для пользователя; (yes)
 * 5. Выдавать ошибку если номер поезда несуществует; (yes)
 * 6. Дорисовать таблицу; (yes)
 * 7. Форматировать вывод в таблице при большем введенном числе
*/