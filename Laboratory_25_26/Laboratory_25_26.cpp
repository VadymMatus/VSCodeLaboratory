// Laboratory_25-26(28-29) C++.cpp
// Завдання: у циклі вводити з клавіатури масиви та обробляти їх, доки на запит “Хочете продовжувати роботу далі(да, ні) ? ” користувач не відповість “нi”.
// Введені лінійні масиви обробляти за заданими алгоритмами а), б) та в), які оформити у вигляді підпрограм(функцій).
// У підпрограмах - функціях ні уведення, ні виведення даних не повинно бути!
// Головна функція повинна вводити дані з клавіатури, викликати  функції - підпрограм на виконання та виводити результати їх роботи.
// Кожного разу виводити також № запуску програми, уведений масив та результати його обробки(за трьома алгоритмами).
// У одномірному масиві, який складається з n цілих елементів, обчислити:
//  а) номер максимального елемента масиву;
//  б) добуток елементів масиву, розташованих між першим та другим нульовими елементами;
//  в) перетворити масив таким чином, щоб у першій його половині розташовувались елементи, що стоять у непарних позиціях, а у другій половині – елементи, що стоять у парних позиціях.

#include <iostream>
#include <string.h>

using namespace std;

int FunPositionMaxElement(const int sizeArray, const int *array, int *positionMaxElement, int &counterPME); /* Функция поиска позиции максимального елемента */
int FunProductBetweenZero(const int sizeArray, const int *array, int &prodBetwZero);		// Функция подсчёта произведения между нулевыми елементами
int FunTransformationArray(const int sizeArray, int *array);					// Функция сортировки массива, согласно пункту в)

#define sizeAnswer 3

int main()
{
	int SizeArray(0);			   // Длина масcива Array
	int CounterPME(0);			   // Счётчик масива PositionMaxElement
	int ProdBetwZero(0);		   // произведение между нулевыми елементами
	int StartProgramm(1);		   // номер запуска программы
	int *PositionMaxElement(NULL); // Позиция максимального елемента(ов)
	int *Array(NULL);			   // Указатель на масив Array
	char Answer[sizeAnswer + 1];   // Ответ на вопрос "Хотите продолжить?"

	do
	{
		system("clear");

		cout << "\t \t \t\033[1;33m <<< ПРОГРАММА ЗАПУСТИЛАСЬ В " << StartProgramm
			 << ((StartProgramm == 1) ? "-ЫЙ РАЗ >>>\033[0m\n" : (StartProgramm == 2) ? "-ОЙ РАЗ >>>\033[0m\n" : "-ИЙ РАЗ >>>\033[0m\n");

		StartProgramm++;

		/*=====================================================*\
					  ВВОД И ПРОВЕРКА ДЛИНЫ МАСИВА
		\*=====================================================*/
		do
		{
			cout << "Введите длину масива: ";
			cin >> SizeArray;

			if (SizeArray <= 0)
			{
				system("clear");
				cout << "Некоректное число!!!" << endl;
			}

		} while (SizeArray <= 0);

		Array = new int[SizeArray];

		/*=====================================================*\
					  ЗАПОСЛЕНИЕ МАСИВА ЧИСЛАМИ
		\*=====================================================*/

		system("clear");

		cout << "\t \t \t\033[1;33m<<< ЗАПОЛНИТЕ МАССИВ ЧИСЛОВЫМИ ЗНАЧЕНИЯМИ >>>\033[0m" << endl;
		for (int i = 0; i < SizeArray; i++)
		{
			cout << "заполните ячейку[" << i + 1 << "]: ";
			cin >> Array[i];
		}

		cout << "\t \t \t  \033[1;33m<<< РЕЗУЛЬТАТЫ ВЫПОЛНЕНИЯ ПРОГРАММЫ >>>\033[0m ";

		/*=====================================================*\
				ВЫВОД ПОЗИЦИИ МАКСИМАЛЬНОГО ЕЛЕМЕНТА(ОВ)
		\*=====================================================*/

		PositionMaxElement = new int[SizeArray]; 

		FunPositionMaxElement(SizeArray, Array, PositionMaxElement, CounterPME); // Функция поиска максимального елемента

		cout << endl
			 << "Позиция максимального елемента(ов): ";

		for (int i = 0; i < CounterPME; i++) // Вывод позиции максимального елемента(ов)
			cout << PositionMaxElement[i] + 1 << ((i == CounterPME - 1) ? "." : ", ");

		/*=====================================================*\
					   ПРОИЗВЕДЕНИЕ МЕЖДУ НУЛЯМИ
		\*=====================================================*/

		cout << endl
			 << "Произведение между первым и вторым нулевыми елементами: ";

		FunProductBetweenZero(SizeArray, Array, ProdBetwZero);

		if (ProdBetwZero != 0)
			cout << ProdBetwZero;

		/*=====================================================*\
						  СОРТИРОВКА МАССИВА
		\*=====================================================*/

		FunTransformationArray(SizeArray, Array);

		cout << endl
			 << "Отсортированый массив: ";

		for (int i = 0; i < SizeArray; i++)
			cout << Array[i] << ((i == SizeArray - 1) ? ".\n" : ", ");

		delete[] Array;
		Array = NULL;

		delete[] PositionMaxElement;
		PositionMaxElement = NULL;

		cin.ignore(); 

		cout << "Хотите продолжить(Д/н)?: ";
		cin.get(Answer, 3);

		while (getchar() != '\n')
			continue;

	} while (strstr(Answer, "д") or strstr(Answer, "Д") or strstr(Answer, "1"));

	return 0;
}

/*=====================================================*\
		ФУНКЦИЯ ПОИСКА ПОЗИЦИИ МАКСИМАЛЬНОГО ЕЛЕМЕНТА
\*=====================================================*/
int FunPositionMaxElement(const int sizeArray, const int *array, int *positionMaxElement, int &counterPME)
{
	int maxElement(0); // Максимальный елемент
	counterPME = 0;

	for (int i = 0; i < sizeArray; i++) // Поиск и запись маскимального елемента
		if (array[i] > maxElement)
			maxElement = array[i];

	for (int i = 0; i < sizeArray; i++) // Запись позиции максимального елемента
		if (array[i] == maxElement)
		{
			positionMaxElement[counterPME] = i;
			counterPME++;
		}

	return 0;
}

/*=====================================================*\
 ФУНКЦИЯ ПОДСЧЁТА ПРОИЗВЕДЕНИЯ МЕЖДУ НУЛЕВЫМИ ЕЛЕМЕНТАМИ
\*=====================================================*/
int FunProductBetweenZero(const int sizeArray, const int *array, int &prodBetwZero)
{
	int arrayIndexZero[2] = {0, 0}; // Позиции нулевых елементов
	int counterAIZ(0);				// Счётчик массива arrayIndexZero

	prodBetwZero = 0;

	for (int i = 0; i < sizeArray; i++)
		if (array[i] == 0)
		{
			arrayIndexZero[counterAIZ] = i;

			if ((++counterAIZ) == 2)
				break;
		}

	//////////////// ПРОВЕРКА НУЛЕЙ В МАСCИВЕ //////////////////

	//cout << "Index1: " << arrayIndexZero[0];
	//cout << "Index2: " << arrayIndexZero[1];
	//cout << "CounterAIZ: " << counterAIZ;

	if (counterAIZ == 0) 
		cout << "В массиве нет нулей!!!";
	else if (counterAIZ == 1) 
		cout << "В массиве только один ноль!!!";
	else if (arrayIndexZero[0] + 1 == arrayIndexZero[1])
		cout << "Нули стоят рядом!!!";
	else 
	{
		prodBetwZero = 1;

		for (int i = arrayIndexZero[0] + 1; i < arrayIndexZero[1]; i++) // подсчёт произведения
			prodBetwZero *= array[i];
	}

	return 0;
}

/*=====================================================*\
			ФУНКЦИЯ СОРТИРОВКИ МАССИВА
\*=====================================================*/
int FunTransformationArray(const int sizeArray, int *array)
{
	int buffer(0); // временная ячейка для сортировки
	int odd(0);

	if (sizeArray % 2 == 1)
		odd = 1;

	for (int finish(sizeArray - odd), start(0); finish > (sizeArray / 2) and start < finish; finish--, start++) // сортировка масива
		for (int i(start); i < finish; i += 2)
		{				  // +========== МЕТОД СОРТИРОВКИ ===========+
			buffer = array[i];	  // | Array[4] = { 0, 1, 2, 3 }             |
			array[i] = array[i + 1];  // +---------------------------------------+
			array[i + 1] = buffer;    // | 0 <-> 1     2     3		     |
		}				  // | 1     0     2 <-> 3   <-- max	     | конец первой итерации
						  // | 1     0 <-> 3     2   <-- max         | конец второй итерации
						  // | 1     3     0     2   <-- finish      |
	return 0;				  // +---------------------------------------+
}
