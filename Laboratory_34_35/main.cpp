/* 
   Завдання на оцінку «4»-«5»: розробити  програму на мові програмування С/С++ для ведення файлу даних структур, яка описана у завданні А лабораторної роботи №30-31.
    Для обслуговування файлу програма повинна мати головне меню з наступними обов’язковими пунктами:
        1) перегляд існуючого файлу даних;
        2) створення нового файлу даних (з підтвердженням згоди на видалення старого файлу);
        3) додавання записів до файлу даних;
        4) видалення записів із файлу даних (з підтвердженням );
        5) пошук (фільтрація) записів за ознакою, що задана  у завданні Б лаб.роботи №30-31 

    Завдання А лабораторної роботи №30-31:
    	А.Описати структуру з ім'ям TRAIN, що містить наступні поля:
        • Назва пункту призначення;
        • Номер поїзда;
        • Час відправлення.
    Ввести з клавіатури дані про 10 поїздах,
    Б. Записи розмістити (і вивести в таблицю) в алфавітному порядку за номерами поїздів;
    В. Написати програму, що виконує наступні дії:
        • Висновок на екран інформації про потяг, номер якого введено з клавіатури;
        • Якщо таких поїздів немає, видати на дисплей видповидне повідомлення.
*/

#include <iostream>
#include <string.h>
#include <fstream>
#include <iomanip>  // для функции setw()
#include <cstdio>   // для функции remove
#include "arrows.h" // ShowCursor() and mygetch()

using namespace std;

#define SizeDestination 256 /* длина масива Destination */
#define FILE_NAME "Train"    /* Има файла */
#define CENTER_TABLE "\n╠═════╬══════════════════════╬══════════════════╬══════════════════════╣"

struct Train
{
    char Destination[SizeDestination]; // Пункт назначения
    long int Number = 0;               // номер поезда
    long int PoisonTime[2] = {0, 0};   // время отправления [0]=часы [1]=минуты
};

int Menu();              // Вывод меню
int FillStruct();        // Ввод даных в файл
int DeleteRoute();       // Удаление маршрута
bool Agreement();        // Запрос на подверждения удаления
bool FileEmpty(string);  // Проверка файла на пустоту
bool FileExists(string); // Проверка файла на существование
int ShowFile(bool);      // Вывод содержимого файла
void SortingFile();      // Сортировка файла

int main()
{
    bool flag(true); // вывод меню пока не false
    
    do
    {
        switch (Menu())
        {
        case 1: // создание файла
        {
            ofstream ofs(FILE_NAME, ios_base::binary | ios_base::out);
            if (ofs.is_open())
            {
                cout << "\033[1;33mSuccessfully!\033[0m Файл создан успешно!\033[2;33m<Enter>...\033[0m";
                ofs.close();
            }
            else
                cout << "\a\033[1;31mFatal error!\033[0m Ошибка создания файла!";

            mygetch();
            break;
        }
        case 2: // просмотр файла
        {
            ShowFile(true);

            mygetch();
            break;
        }
        case 3: // добавление маршрутов
        {
            FillStruct();

            break;
        }
        case 4: // удаление маршрутов
        {
            DeleteRoute();

            break;
        }
        case 5: // поиск маршрутов
        {
            ShowFile(false);

            break;
        }
        case 6: // удаление файла
        {
            if (remove(FILE_NAME))
                cout << "\a\033[1;31mFatal error! \033[0mОшибка удаления, возможно файла несуществует!\033[2;33m<Enter>...\033[0m";
            else
                cout << "\033[1;33mSuccessfully!\033[0m Файл удалён успешно!\033[2;33m<Enter>...\033[0m";

            mygetch();
            break;
        }
        case 7: // выход
        {
            cout << "\033[1;31mExit!\033[0m Завершено пользователем!\033[2;33m<Enter>...\033[0m" << '\n';

            flag = false;
            break;
        }
        default:
        {
            cout << "\a\033[1;31mFatal error! \033[0mНеизвестная ошибка!\n";
            cout << "\033[1;31mAbort!\033[0m Екстреное завершение програмы!\033[2;33m<Enter>...\033[0m" << '\n';

            flag = false;
            break;
        }
        }

    } while (flag);

    mygetch();
    return 0;
}

/*===================================================*\
         ФУНКЦИЯ ВЫВОДА МЕНЮ И ПРОВЕРКИ ОТВЕТА
\*===================================================*/
int Menu()
{
    int answer(0);             // ответ пользователя на меню
    static int8_t counter(1);  // счётчик для меню
    static bool repeat(false); // показывать меню ещё раз или нет

    do
    {
        ShowCursor(false); // скрываю курсор

        do
        {
            system("clear");

            cout << "   \033[1;33m<<< М Е Н Ю >>>\033[0m\n\n"
                 << ((counter == 1) ? "\033[1;31m-> \033[1;31m" : "   ") << "Создание файла"
                 << ((counter == 1) ? "\033[1;31m <-\033[0m" : "   ") << '\n'
                 << ((counter == 2) ? "\033[1;31m-> \033[1;131m" : "   ") << "Просмотр файла"
                 << ((counter == 2) ? "\033[1;31m <-\033[0m" : "   ") << '\n'
                 << ((counter == 3) ? "\033[1;31m-> \033[1;31m" : "   ") << "Добавить маршрут"
                 << ((counter == 3) ? "\033[1;31m <-\033[0m" : "   ") << '\n'
                 << ((counter == 4) ? "\033[1;31m-> \033[1;31m" : "   ") << "Удалить маршрут"
                 << ((counter == 4) ? "\033[1;31m <-\033[0m" : "   ") << '\n'
                 << ((counter == 5) ? "\033[1;31m-> \033[1;31m" : "   ") << "Поиск маршрутов"
                 << ((counter == 5) ? "\033[1;31m <-\033[0m" : "   ") << '\n'
                 << ((counter == 6) ? "\033[1;31m-> \033[1;31m" : "   ") << "Удалить файл"
                 << ((counter == 6) ? "\033[1;31m <-\033[0m" : "   ") << '\n'
                 << ((counter == 7) ? "\033[1;31m-> \033[1;31m" : "   ") << "Выйти"
                 << ((counter == 7) ? "\033[1;31m <-\033[0m" : "   ") << '\n'
                 << "\n\033[0;35mУправление\033[1;33m(↑↓)\033[0;35m\n"
                 << "Выбор\033[1;33m<Enter>\033[0m";

            answer = mygetch();

            if (answer == KEY_DOWN and counter < 8)
                counter++;
            else if (answer == KEY_UP and counter > 0)
                counter--;

            /* Осуществление кругового выбора */
            if (answer == KEY_DOWN and counter == 8)
                counter = 1;
            else if (answer == KEY_UP and counter == 0)
                counter = 7;

        } while (answer != ENTER);

        ShowCursor(true); // показываю курсор

        repeat = (counter > 0 and counter < 8) ? true : false;

        switch (counter)
        {
        case 1: // создать новый файл
        {
            cout << "\033[1;31mCaution!\033[0m Создать новый файл и удалить старый, если он существует(y/N)?: ";

            if (Agreement())
                return counter;
            else
                cout << "\a\033[1;31mAbort!\033[0m Отмена операции!\033[2;33m<Enter>...\033[0m";

            break;
        }
        case 2: // просмотр файла
        {
            if (FileExists(FILE_NAME))     // проверка на существование
                if (!FileEmpty(FILE_NAME)) // проверка на пустоту
                    return counter;
                else
                    cout << "\a\033[1;31mError!\033[0m Файл пустой!\033[2;33m<Enter>...\033[0m";
            else
                cout << "\a\033[1;31mError!\033[0m Файла не существует!\033[2;33m<Enter>...\033[0m";

            break;
        }
        case 3: // добавить маршрут
        {
            if (FileExists(FILE_NAME)) // проверка на существование
                return counter;
            else
                cout << "\a\033[1;31mError!\033[0m Файла не существует!\033[2;33m<Enter>...\033[0m";

            break;
        }
        case 4: // удалить маршрут
        {
            if (FileExists(FILE_NAME))     // проверка на существование
                if (!FileEmpty(FILE_NAME)) // проверка на пустоту
                    return counter;
                else
                    cout << "\a\033[1;31mError!\033[0m Файл пустой!\033[2;33m<Enter>...\033[0m";
            else
                cout << "\a\033[1;31mError!\033[0m Файла не существует!\033[2;33m<Enter>...\033[0m";

            break;
        }
        case 5: // поиск маршрутов
        {
            if (FileExists(FILE_NAME))     // проверка на существование
                if (!FileEmpty(FILE_NAME)) // проверка на пустоту
                    return counter;
                else
                    cout << "\a\033[1;31mError!\033[0m Файл пустой!\033[2;33m<Enter>...\033[0m";
            else
                cout << "\a\033[1;31mError!\033[0m Файла не существует!\033[2;33m<Enter>...\033[0m";

            break;
        }
        case 6: // удалить файл
        {
            cout << "\033[1;31mCaution!\033[0m Удалить файл(y/N)?: ";

            if (Agreement()) // подтверждение
                return counter;
            else
                cout << "\a\033[1;31mError!\033[0m Отмена операции!\033[2;33m<Enter>...\033[0m";

            break;
        }
        case 7: // выход
            return counter;
        default:
        {
            cout << "\a\033[1;31mFatal error!\033[0m Неизвестная ошибка!";

            break;
        }
        }

        mygetch();

    } while (repeat);

    return 0;
}

/*===================================================*\
                ФУНКЦИЯ ВВОДА ДАНЫХ
\*===================================================*/
int FillStruct()
{
    Train train, *Tp = &train;

    bool repeat(false); // повторять ли ввод если он завершился неудачно
    int spare(0);       // количество пробелов в строке

    cout << "\033c \t\t\t\033[1;33m<< ЗАПОЛНИТЕ МАРШРУТ >> \033[0m\n";

    ////////////// Ввод пункта назначения ///////////////
    do
    {
        repeat = false;

        cout << "Пункт назначения \033[1;34m-> \033[0m";

        if (!(cin.getline(Tp->Destination, 20)))
        {
            cin.clear();
            while (cin.get() != '\n')
                continue;
        }

        if (strlen(Tp->Destination) == 0 or Tp->Destination[0] == '.')
        {
            cout << "\a\033[1;31mError!\033[0m Название пустое!\n";
            repeat = true;
        }
        else
        {
            spare = 0;

            for (int j(0); j < (strlen(Tp->Destination)); j++)
                if (Tp->Destination[j] == ' ' or Tp->Destination[j] == '.')
                    spare++;

            if (spare == (strlen(Tp->Destination)))
            {
                cout << "\033[1;31mIncorrectly!\033[0m Название состоит только из пробелов!\n";

                repeat = true;
            }
        }

    } while (repeat);

    ////////////// Ввод номера поезда //////////////////
    do
    {
        cout << "Номер поезда \033[1;34m-> \033[0m";

        while (!(cin >> Tp->Number))
        {
            cin.clear();
            while (cin.get() != '\n')
                continue;

            cout << "\a\033[1;31mFatal Error!\033[0m Введите число!\n"
                 << "\rНомер поезда \033[1;34m-> \033[0m";
        }
        if (Tp->Number < 0)
            cout << "\a\033[1;31mFatal Error!\033[0m Номер поезда не может быть отрицательным!\n";

        cin.clear();
        while (cin.get() != '\n')
            continue;

    } while (Tp->Number < 0);

    ////////////// Ввод время отправления ///////////////
    do
    {
        cout << "Время отправления через пробел \033[1;34m-> \033[0m";

        while (!(cin >> Tp->PoisonTime[0] >> Tp->PoisonTime[1]))
        {
            cin.clear();
            while (cin.get() != '\n')
                continue;

            cout << "\a\033[1;31mFatal Error!\033[0m Введите целочисленое значение!"
                 << "\nВремя отправления через пробел \033[1;34m-> \033[0m ";
        }

        if (Tp->PoisonTime[0] > 24 or Tp->PoisonTime[0] < 0 or Tp->PoisonTime[1] > 59 or Tp->PoisonTime[1] < 0 or Tp->PoisonTime[0] == 24 and Tp->PoisonTime[1] > 0)
            cout << "\a\033[1;31mError!\033[0mВведите коректное время!\n";

    } while (Tp->PoisonTime[0] > 24 or Tp->PoisonTime[0] < 0 or Tp->PoisonTime[1] > 59 or Tp->PoisonTime[1] < 0 or Tp->PoisonTime[0] == 24 and Tp->PoisonTime[1] > 0);

    ////////////// Запись структуры в файл //////////////
    ofstream ofs(FILE_NAME, ios_base::binary | ios_base::app);

    ofs.write((char *)Tp, sizeof(*(Tp)));

    ofs.close();

    SortingFile(); // сразу сортирую файл

    cin.clear();
    while (cin.get() != '\n')
        continue;

    cout << "Заполнить ещё один маршрут(y/N)?: ";

    if (Agreement())
        if (!FillStruct())
            return 0;
        else
            cout << "\a\033[1;31mFatal Error!\033[0m Неизвестная ошибка!\033[2;33m<Enter>\033[0m";
    else
        return 0;
}

/*===================================================*\
       ФУНКЦИЯ ЗАПРОСА ПОДТВЕРЖДЕНИЯ НА УДАЛЕНИЕ
\*===================================================*/
bool Agreement() // true = подтвердить
{                // false = отмена
    char YesNo;
    bool flag(false);

    cin.get(YesNo);

    if (YesNo == '\n')
        return flag;
    else if (YesNo == 'N' or YesNo == 'n')
        flag = false;
    else if (YesNo == 'Y' or YesNo == 'y')
        flag = true;
    else
        flag = false;

    cin.clear();
    while (cin.get() != '\n')
        continue;

    return flag;
}

/*===================================================*\
       ФУНКЦИЯ ПРОВЕРКИ ФАЙЛА НА СУЩЕСТВОВАНИЕ
\*===================================================*/
bool FileExists(string FileName) // true = файл существует
{                                // false = файл не существует
    bool flag(false);

    ifstream ifs(FileName, ios::binary | ios_base::ate);

    flag = ((ifs.is_open()) ? true : false);

    if (flag)
        ifs.close();

    return flag;
}

/*===================================================*\
       ФУНКЦИЯ ПРОВЕРКИ ФАЙЛА НА ПУСТОТУ
\*===================================================*/
bool FileEmpty(string FileName) // false = файл не пустой
{                               // true = файл пустой
    bool flag(false);

    ifstream ifs(FileName, ios_base::binary | ios_base::ate);

    flag = (ifs.tellg() == 0) ? true : false;

    ifs.close();
    return flag;
}

/*===================================================*\
          ФУНКЦИЯ ВЫВОДА СОДЕРЖИМОГО ФАЙЛА
\*===================================================*/
int ShowFile(bool more = false) // false = вывод за номером
{                               // true = вывести все маршруты
    Train Tp;

    int64_t count(0); // количетво записей в файле
    int distant(0);   // для ровного вывода даных в таблице
    int number(0);    // номер поезда для поиска
    bool flag(true);  // есть ли поезд в базе данных

    do
    {
        if (more)
        {
            cout << "\033c \033[1;33m\t\t\t<<< МАРШРУТЫ ВСЕХ ПОЕЗДОВ >>>\033[0m\v\n";
            break;
        }

        cout << "\033c \033[1;33m\t\t\t<<< ПОИСК МАРШРУТОВ >>>\033[0m\v\v\rВведите номер поезда \033[1;34m-> \033[0m";

        while (!(cin >> number))
        {
            cin.clear();
            while (cin.get() != '\n')
                continue;

            cout << "\a\033[1;31mFatal Error!\033[0m Введите целочисленое значение!"
                 << "\nВведите номер поезда \033[1;34m-> \033[0m";
        }

        while (cin.get() != '\n')
            continue;

        if (number < 0)
        {
               // system("clear");
            cout << "\a\033с\033[1;31mError!\033[0m Номер поезда, неможет быть отрицательным!";
            getchar();
        }

    } while (number < 0);

    ////////////////// ВЫВОД ТАБЛИЦЫ С ДАННЫМИ /////////////////
    ifstream ifs(FILE_NAME, ios::binary | ios::in);

    cout << "╔═════╦══════════════════════╦══════════════════╦══════════════════════╗"
         << "\n║  \033[1;33m№\033[0m  ║   \033[1;33mПункт назначения\033[0m   ║   \033[1;33mНомер поезда\033[0m   ║   \033[1;33mВремя отправления\033[0m  ║"
         << CENTER_TABLE;

    while (ifs.read((char *)&Tp, sizeof(Tp)))
    {
        if (!more and Tp.Number != number)
            continue;

        distant = 25 - strlen(Tp.Destination);

        cout << "\n║" << setw(3) << ++count << "  ║ " << Tp.Destination << ((strlen(Tp.Destination) > 17) ? "\b\b\b..." : "") << setw(distant) << "║ "
             << setw(9) << Tp.Number << "\t║ "
             << setw(10) << Tp.PoisonTime[0] << ((Tp.PoisonTime[1] < 10) ? ":0" : ":") << Tp.PoisonTime[1] << "\t       ║"
             << CENTER_TABLE;

        flag = false;
    }

    cout << ((flag) ? "\n║     ║     \033[1;31mНет данных!\033[0m      ║    \033[1;31mНет данных!\033[0m   ║      \033[1;31mНет данных!\033[0m     ║\n" : "")
         << "\r╚═════╩══════════════════════╩══════════════════╩══════════════════════╝"
         << ((flag) ? "\n\033[1;31mНет поезда с таким номером!\033[0m\n" : "\n")
         << "Для продолжения нажмите\033[2;33m<Enter>...\033[0m";

    ifs.close();

    ////////////// Запрос на продолжение ////////////
    if (!more)
    {
        cout << "\rПродолжить поиск маршрутов(y/N)?: ";

        if (Agreement())
            if (!(ShowFile(false)))
                return 0;
            else
                cout << "\a\033[1;31mFatal Error!\033[0m Неизвестная ошибка!\033[2;33m<Enter>\033[0m";
        else
            return 0;
    }
}

/*===================================================*\
             ФУНКЦИЯ УДАЛЕНИЯ МАРШРУТА
\*===================================================*/
int DeleteRoute()
{
    int64_t count(0);
    int answer(0);

    ifstream ifs(FILE_NAME, ios_base::binary | ios_base::ate);
    count = ((int64_t)ifs.tellg()) / sizeof(Train); // узнаю сколько записей в файле

    ifs.seekg(0, ios_base::beg); // устанавливаю указатель на начало

    Train Tp[count];

    ifs.read(reinterpret_cast<char *>(Tp), count * sizeof(Tp[0]));

    ifs.close();

    ShowFile(true); // вывожу все доступные записи

    do
    {
        cout << "\rВведите номер записи для удаления \033[1;34m-> \033[0m";

        if (!(cin >> answer))
            cout << "\a\033[1;31mFatal error!\033[0m Введите целочисленое значение!\n";
        else if (answer < 1 or answer > count)
            cout << "\a\033[1;31mIncorectly!\033[0m Выберите доступную запись!\n";

        cin.clear();
        while (getchar() != '\n')
            continue;

    } while (answer < 1 or answer > count);

    cout << "\033[1;31mCaution!\033[0m Вы действительно хотите удалить запись(y/N)?: ";

    if (Agreement())
    {
        for (int i(answer - 1); i < count; i++)
            Tp[i] = Tp[i + 1];

        ofstream ofs(FILE_NAME, ios_base::binary | ios_base::trunc);

        ofs.write(reinterpret_cast<char *>(Tp), (count - 1) * sizeof(Tp[0]));

        ofs.close();

        cout << "\033[1;33mSuccessfully!\033[0m Запись успешно удалена!\033[2;33m<Enter>...\033[0m";
    }
    else
        cout << "\a\033[1;31mAbort!\033[0m Отмена операции!\033[2;33m<Enter>...\033[0m";

    cout << "\nУдалить ещё один маршрут(y/N)?: ";

    if (Agreement())
        if (!DeleteRoute())
            return 0;
        else
            cout << "\a\033[1;31mFatal Error!\033[0m Неизвестная ошибка!\033[2;33m<Enter>\033[0m";
    else
        return 0;
}

/*===================================================*\
              ФУНКЦИЯ СОРТИРОВКИ ФАЙЛА
\*===================================================*/
void SortingFile() // Удаление маршрута
{
    Train Sorting; // будет хранить елемент масива структур при сортировки
    int64_t count(0);

    ifstream ifs(FILE_NAME, ios_base::binary | ios_base::ate);
    count = ((int64_t)ifs.tellg()) / sizeof(Train); // узнаю сколько записей в файле

    ifs.seekg(0, ios_base::beg);
    Train Tp[count];

    ifs.read(reinterpret_cast<char *>(Tp), count * sizeof(Tp[0]));
    ifs.close();
    
metka:
    for (int i = 0; i < count - 1; i++)
        if (strcmp(Tp[i].Destination, Tp[i + 1].Destination) > 0)
        {
            Sorting = Tp[i];
            Tp[i] = Tp[i + 1];
            Tp[i + 1] = Sorting;

            goto metka;
        }

    ofstream ofs(FILE_NAME, ios_base::binary | ios_base::trunc); // предварительно удаляю всё содержимое
    ofs.write(reinterpret_cast<char *>(Tp), count * sizeof(Tp[0]));
    ofs.close();
}
