#if !defined ARROWS_H
#define ARROWS_H

#include <iostream>
#include <termios.h>
#include <unistd.h>

enum
{
    KEY_UP = 183,
    KEY_DOWN = 184,
    KEY_LEFT = 186,
    KEY_RIGHT = 185,
    ENTER = 10
};

int mygetch(void)
{
    struct termios oldt, newt;
    int ch;
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    ch = getchar();

    if (ch == ENTER)
        return ch;
    else if (ch != 27)
        return 0;

    ch += getchar() + getchar();
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);

    return ch;
}

void ShowCursor(bool flag) // false = скрыть курсор
{                          // true = показать курсор
    system("tput reset ");

    system((flag) ? "tput cnorm" : "tput civis");
}

#endif